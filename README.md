# screenshot-maim-upload

My setup on screenshotting with maim and then uploading via scp to my server. `screenshot.sh` needs to be set executable via `chmod +x` and put into a hotkey, in linux mint that's done via `Keyboard Shortcuts` on kde there's `Custom Hotkeys` or something alike, every DE has something like that, just google.

Makes use of: `pa-play`, `notify-send`, `maim`, `scp`