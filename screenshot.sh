#!/bin/bash

# bypass linux mint keyboard shortcut weirdness
sleep .1

# you might not need this, but keepassxc uses keyring for the ssh-agent
# check settings in keepassxc where yours is or comment it out entirely
# if you've got other use-case (e.g. not uploading the screenshots at all)
export SSH_AUTH_SOCK=/run/user/1000/keyring/ssh;

# alternatives for just clipboard screenshotting, no upload
# make sure to comment out everything else below if you want to use this instead
    # mate-screenshot --area --clipboard
    # /usr/bin/flatpak run --branch=stable --arch=x86_64 --command=flameshot org.flameshot.Flameshot gui -r | xclip
    # maim -sou --color=255,0,0,0.6 | xclip -selection clipboard -t image/png

randomfilename=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32 ; echo '')
maim -sou --color=255,0,0,0.6 "/tmp/$randomfilename.png"

# replace sshalias with whatever ssh server alias you want to upload to
# see ~/.ssh/config and replace the path with the on-server path
scp "/tmp/$randomfilename.png" sshalias:/path/to/screenshot/folder

if [ $? -eq 0 ];
then
    # replace link with whatever the fuck your public facing URL is to the file
    printf "https://yourdomain.com/$randomfilename.png" | xclip -sel clip

    # download any .ogg and put the filepath here
    # alternatively uncomment the notify-send below and comment
    # the paplay out
    paplay --volume=30000 /path/to/sound/notification.ogg
    
    #notify-send 'Uploaded!'
else
    notify-send 'Error..'
fi

rm "/tmp/$randomfilename.png"